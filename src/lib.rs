//! Dummy implementation of `embedded_tracing_logger`.
//!
//! This crate is required to satisfy linking requirements for `embedded_tracing` (client library).
//! It defines the interface of `embedded_tracing_logger`. The logger used in the embedded application must satisfy this interface.
//!
//! It is important that a chosen logger implementation [overrides](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html) this crate by using patching.
//!
//! The dummy implementation is no-op.

#![no_std]
#![deny(unsafe_code)]
#![warn(missing_docs)]

use embedded_tracing_types::TracePoint;

/// Log [TracePoint](embedded_tracing_types::TracePoint) in the logger.
pub fn log_trace_point(_trace_point: TracePoint) {}

#[cfg(test)]
mod tests {
    use super::log_trace_point;
    use embedded_tracing_types::{Id, Timestamp, TraceLevel, TracePoint, TraceType};
    #[test]
    fn test_log_trace_point() {
        let trace_point = TracePoint::from(
            Timestamp::from_value(0),
            TraceType::Event,
            TraceLevel::Debug,
            Id::from_value(0),
        );
        log_trace_point(trace_point);
    }
}
