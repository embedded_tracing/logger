# embedded_tracing_logger - Readme

crate that contains the dummy implementation of `embedded_tracing_logger` which is required during linkage of `embedded_tracing` (client library). It defines the `embedded_tracing_logger` interface and realizes it with a "no-op" implementation.

## Usage

This crate is not used actually but [overridden](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html) by a logger implementation which is specific for the embedded hardware etc..

## License

MIT, see [LICENSE](./LICENSE) file.
